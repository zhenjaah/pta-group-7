import wikipedia
import glob
import fnmatch
from nltk import word_tokenize, pos_tag
from nltk.corpus import wordnet
from nltk.wsd import lesk
def main ():

    listNouns = []

    path_list = glob.glob("Finalproject/dev/p05/*/*.tok.off.pos")
    for path in path_list:
        f = open(path)

        for regel in f:
            regel = regel.rstrip()
            if fnmatch.fnmatch(regel.split(" ")[4], 'NN*'):
                listNouns.append(regel.split(" ")[3])

    print(listNouns)

    for p in listNouns: #hier wordt door alle nouns heen geloopt, maar dit moet alleen bij de woorden waar ook een tag achter komt te staan
        try:
            page = wikipedia.page(p)
            print(page.url) #die moet dus geprint worden in de file met tags etc
        except wikipedia.exceptions.DisambiguationError as e:
            try:
                page = wikipedia.page(e.options[1]) #kies de eerste optie van de disamb page
                print(page.url)
            except wikipedia.exceptions.DisambiguationError as h: #error afvangen wanneer die eerste optie ook een disamb page heeft
                print("https://en.wikipedia.org/wiki/"+p)
            except wikipedia.PageError:
                print("https://en.wikipedia.org/wiki/")

main()
