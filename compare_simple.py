import csv
import glob
from collections import Counter
from nltk.metrics import ConfusionMatrix

"""Dit is voor het creeren van een de kleine, simpele confusion matrixen."""
def main():
    lijstje = []
    lijstje_2 = []
    path_list = glob.glob("group7/*/*/*.tok.off.pos")
    for path in path_list:
        with open(path) as csv_file:
            for line in csv_file:
                line = line.split()
                if len(line) > 5:
                    lijstje.append("w")
                else:
                    lijstje.append("x")


    path_list = glob.glob("group7.4/*/*/*.tok.off.pos")
    for path in path_list:
        with open(path) as csv_file:
            for line in csv_file:
                line = line.split()
                if len(line) > 5:
                    lijstje_2.append("w")
                else:
                    lijstje_2.append("x")


    ref  = lijstje
    tagged = lijstje_2
    cm = ConfusionMatrix(ref, tagged)

    print(cm)

    labels = set('w'.split())

    true_positives = Counter()
    false_negatives = Counter()
    false_positives = Counter()

    for i in labels:
        for j in labels:
            if i == j:
                true_positives[i] += cm[i,j]
            else:
                false_negatives[i] += cm[i,j]
                false_positives[j] += cm[i,j]

    print("TP:", sum(true_positives.values()), true_positives)
    print("FN:", sum(false_negatives.values()), false_negatives)
    print("FP:", sum(false_positives.values()), false_positives)
    print() 

    for i in sorted(labels):
        if true_positives[i] == 0:
            fscore = 0
        else:
            precision = true_positives[i] / float(true_positives[i]+false_positives[i])
            recall = true_positives[i] / float(true_positives[i]+false_negatives[i])
            fscore = 2 * (precision * recall) / float(precision + recall)
        print(i, fscore)
main()

