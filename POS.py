#!/usr/bin/python3
# Name: POS.py
# A program that complies to exercise 2 of PTA assignment 2.
# Authors: Thimo, Timo and Zhenja
# Date: 06-05-2019


import nltk
import collections

def wordCounter(words):
    """This function checks the frequency of words"""

    wordcount = {}

    for word in words:
        if word not in wordcount:
            wordcount[word] = 1
        else:
            wordcount[word] += 1

    word_counter = collections.Counter(wordcount)
    
    return word_counter
    

def main():

    br_tw = nltk.corpus.brown.tagged_words(categories="mystery")
    br_ts = nltk.corpus.brown.tagged_sents(categories="mystery")

    print("2A.1) This corpus has {} words.\n".format(len(br_tw)))
    print("2A.2) This corpus has {} sentences.\n".format(len(br_ts)))
    print("2B.1) The 50th word in the corpus and its POS are: {}.\n".format(br_tw[49]))
    print("2B.2) The 75th word in the corpus and its POS are: {}.\n".format(br_tw[74]))

    taglist = []

    for tag in br_tw:
        taglist.append(tag[1])
    print("2C) There are {} different pos in this corpus.\n".format(len(set(taglist))))

    words = []

    for word in br_tw:
        words.append(word[0])

    word_counter = wordCounter(words)
    print("2D) 15 most frequently used words in the corpus:")
    for word_counter in word_counter.most_common(15):
        print("{0} occurs {1} times.".format(word_counter[0], word_counter[1]))

    words = []

    for word in br_tw:
        words.append(word[1])

    word_counter = wordCounter(words)
    print("\n2E) 15 most frequently used POS tags in the corpus:")
    for word_counter in word_counter.most_common(15):
        print("{0} occurs {1} times.".format(word_counter[0], word_counter[1]))

        
    words = []

    for word in br_ts[19]:
        words.append(word[1])
    
    word_counter = wordCounter(words)
    print("\n2F.1) The most frequent POS tag in the 20th sentence of the corpus:")
    for word_counter in word_counter.most_common(1):
        print("{0} occurs {1} times.".format(word_counter[0], word_counter[1]))

    words = []

    for word in br_ts[39]:
        words.append(word[1])

    word_counter = wordCounter(words)

    print("\n2F.2) The most frequent POS tag in the 40th sentence of the corpus:")
    for word_counter in word_counter.most_common(1):
        print("{0} occurs {1} times.".format(word_counter[0], word_counter[1]))

    words = []

    for word in br_tw:
        if word[1] == "RB":
            words.append(word)

    word_counter = wordCounter(words)

    print("\n2G) Most frequently used adverb in the corpus:")
    for word_counter in word_counter.most_common(1):
        print("{0} occurs {1} times.".format(word_counter[0][0], word_counter[1]))


    words = []

    for word in br_tw:
        if word[1] == "JJ":
            words.append(word)

    word_counter = wordCounter(words)

    print("\n2H) Most frequently used adjective in the corpus:")
    for word_counter in word_counter.most_common(1):
        print("{0} occurs {1} times.\n".format(word_counter[0][0], word_counter[1]))

    so_pos = []

    for so in br_tw:
        if so[0] == "so":
            so_pos.append(so[1])

    print("2I) The three POS of 'so' are: {}.\n".format(set(so_pos)))

    so_pos = []

    for so in br_tw:
        if so[0] == "so":
            so_pos.append(so[1])

    print("2J) The most commonly used POS tag for 'so': {}.\n".format(max(set(so_pos), key=so_pos.count)))

    sents_list = []
    POS_list = []

    for sents in br_ts:
        for word in sents:
             if word[0] == "so":
                    if word[1] == "QL":
                        if word[1] not in POS_list:
                            sents_list.append(sents)
                            POS_list.append(word[1])
                    if word[1] == "CS":
                        if word[1] not in POS_list:
                            sents_list.append(sents)
                            POS_list.append(word[1])
                    if word[1] == "RB":
                        if word[1] not in POS_list:
                            sents_list.append(sents)
                            POS_list.append(word[1])
    print("2K) Example sentences of each POS of 'so' are:")
    print(sents_list[0], "\n\n", sents_list[1], "\n\n", sents_list[2])

           
    print("\n2L) This exercise was not clear to us. We did not know how to solve this one.")

    with open("holmes.txt") as f:
        lines = f.readlines()
        lines = "".join(lines)
        tokenized = nltk.word_tokenize(lines)
        all_pos_tags = nltk.pos_tag(tokenized)
    print(all_pos_tags)
    counts = collections.Counter(all_pos_tags)
    print(sorted(all_pos_tags, key=counts.get, reverse = True))
    
    with open("holmes.txt") as f:
        lines = f.read()
        tokenized = nltk.word_tokenize(lines)
        bgs = nltk.bigrams(tokenized)
        fdist_bgs = nltk.FreqDist(bgs)
        mst_common_big = fdist_bgs.most_common(5)
        print(mst_common_big)
        # The 5 most frequent bigrams do not look interesting, because they
        # are mostly just punctuation. The frequency of the bigrams of POS 
        # tags is somewhat similar. These particular bigrams are not really 
        # usefull for anything.

    
if __name__ == "__main__":
    main()

