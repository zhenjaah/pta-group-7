import spacy
import glob

nlp = spacy.load("en_core_web_sm")

paths_list = []
path_list = glob.glob("Finalproject/dev/*/*/*.tok.off.pos")
for path in path_list:
    with open(path) as f:
        lines = f.readlines()
        words = []
        lst = []
        for x in lines:
            words.append(x.split(" ")[3])
        words = " ".join(words)
        words = nlp(words)
        for ent in words.ents:
            paths_list.append(ent.label_)
for ent in paths_list:
    if ent == "WORK_OF_ART":
        print("ENT")
    else:
        print("O")
