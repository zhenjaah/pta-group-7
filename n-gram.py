#!/usr/bin/python3
# Name: n-gram.py
# A program that complies to exercise 2 of PTA assignment 1.
# Authors: Thimo, Timo and Zhenja
# Date: 18-04-2019


from nltk.tokenize import sent_tokenize, word_tokenize
from itertools import chain
from collections import Counter
from nltk.util import ngrams
import nltk


def textFixer():

    """This function opens the text file and tokenizes the lines into sentences and words"""

    with open("holmes.txt") as f:
        # Make a string of the lines
        lines = f.readlines()
        lines = "".join(lines)
        
        # Tokenize lines into sentences
        sentences = lines.split(". ")
        sentences = sent_tokenize(lines)

        # Split each sentence into words
        tokenized_sentences = [word_tokenize(sentence) for sentence in sentences]

    return tokenized_sentences, sentences, sorted(set(lines))


def sentenceLength():
       
    """This function calculates the longest and the shortest sentence"""
     
    tokenized_sentences = textFixer()[0]

    # Determine longest sentence (at random)
    longest_sentence = max(tokenized_sentences, key=len)
    lenght_of_longest_sen = len(longest_sentence)

    # Determine shortest sentence (at random)
    shortest_sentence = min(tokenized_sentences, key=len)
    length_of_shortest_sen = len(shortest_sentence)

    return longest_sentence, shortest_sentence

def averageSentenceLength():

    """This function calculates the average sentence length"""

    word_count = []
    sentences = textFixer()[1]
    for sentence in sentences:
        words = sentence.split(" ")
        word_count.append(len(words))
    avg = sum(word_count)/len(word_count)
    
    return avg

def typesCounter():

    """This function counts the amount of different types of characters and words"""
   
    #Character types are counted here
    char_types = textFixer()[2]
    count = 0
    for ch in char_types:
        count += 1

    #Word types are counted here
    wordtypes_frequency = []
    sentences = textFixer()[1]
    wordtypes = chain(*[word_tokenize(sentence) for sentence in sentences])
    wordtypes_counted = Counter(wordtypes)
    
    for i in sorted(wordtypes_counted.keys()):  
        wordtypes_frequency.append(i)

    return wordtypes_frequency, len(wordtypes_frequency), char_types, count

def ngramDistribution():

    """This function finds the 20 most and least common uni/bi/trigrams"""

    with open("holmes.txt") as f:
        lines = f.read()
   
        # tokenizing the the text
        tokens = nltk.word_tokenize(lines)
   
        # making uni/bi/trigrams from the tokens
        ugs = ngrams(tokens, 1)
        bgs = nltk.bigrams(tokens)
        tgs = nltk.trigrams(tokens)
        
        #sorting the uni/bi/trigrams in order from most common to least common
        fdist_tgs = nltk.FreqDist(tgs)
        mst_common_tri = fdist_tgs.most_common(20)
        lst_common_tri = fdist_tgs.most_common()[-20:]
      
        fdist_bgs = nltk.FreqDist(bgs)
        mst_common_big = fdist_bgs.most_common(20)
        lst_common_big = fdist_bgs.most_common()[-20:]

   
        fdist_ugs = nltk.FreqDist(ugs)
        mst_common_uni = fdist_ugs.most_common(20)
        lst_common_uni = fdist_ugs.most_common()[-20:]
        
        # The most used tri/bi/unigrams give an example of commonly used words and word structures that is used in the text file.
        # The least used tri/bi/unigrams are mostly words that give meaning because they aren't used a lot.

    return mst_common_tri, lst_common_tri, mst_common_big, lst_common_big, mst_common_uni, lst_common_uni

def sentenceDistribution():

    """This function returns the amount of sentences with a specific length"""

    # a list and dictionary for the length of sentences and the frequencies
    sentence_length_list = []
    freq= {}

    sentences = textFixer()[1]
          
    # tokenizing the text in sentences and adding the length of the sentence to a list   
    for element in sentences:
        tokenized_sentences = word_tokenize(element)
        sentence_length = len(tokenized_sentences)
        sentence_length_list.append(sentence_length)
        
    # adding the sentence length to a dictionary to keep count of the frequency of sentence length
    for item in sentence_length_list:
        if (item in freq):
            freq[item] += 1
        else:
            freq[item] = 1
             
    return freq


def main():

    assignment_a_b = sentenceLength()
    assignment_d = averageSentenceLength()
    assignment_2ab = typesCounter()
    assignment_2cd = ngramDistribution()
    assignment_1c = sentenceDistribution()

    print("The longest sentence is:\n{}\n".format(assignment_a_b[0]))
    print("The shortest sentence is:\n{}\n".format(assignment_a_b[1]))
    print("The average sentence length is:\n{}".format(assignment_d))

    print("The character types, alphabetically ordered, are:\n{}.\n".format(assignment_2ab[2]))
    print("The total amount of character types is: {}.\n".format(assignment_2ab[3]))
    print("The word types, alphabetically ordered, are:\n{}\n".format(assignment_2ab[0]))
    print("The total amount of word types is: {}.\n".format(assignment_2ab[1]))

    print("The 20 most common trigrams are:\n{}\n".format(assignment_2cd[0]))
    print("The 20 least common trigrams are:\n{}\n".format(assignment_2cd[1]))

    print("The 20 most common bigrams are:\n{}\n".format(assignment_2cd[2]))
    print("The 20 least common bigrams are:\n{}\n".format(assignment_2cd[3]))

    print("The 20 most common unigrams are:\n{}\n".format(assignment_2cd[4]))
    print("The 20 least common unigrams are:\n{}\n".format(assignment_2cd[5]))

    print("Length : Amount of sentences")
    print("The distribution of sentences in terms of length are:\n{}".format(assignment_1c))
    


if __name__ == "__main__":
    main()

