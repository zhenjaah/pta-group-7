#!/usr/bin/python3
# Name: add-col-5.py
# A program that complies to exercise 1 of PTA assignment 4.
# Authors: Thimo, Timo, Durk and Zhenja
# Date: 21-05-2019

import glob
import csv
from nltk import word_tokenize, pos_tag


def main():

    path_list = glob.glob("group7/*/*/*.tok.off")
    for path in path_list:
        with open(path) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=" ")
            for line in csv_reader:
                word = line[3]
                tokenized = word_tokenize(word)
                pos = pos_tag(tokenized)
                for postag in pos:
                    line.append(postag[1])
                print(" ".join(line))


if __name__ == "__main__":
    main()
