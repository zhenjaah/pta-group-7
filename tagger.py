# File name: tagger.py
# Authors: Durk Betsem, Thimo Huisman, Zhenja Gnezdilov, Timo Strijbis,
# Date: June 17
"""Usage: For this program to work, you need to have the
stanford coreNLP package and have this as your directory.
The stanford package can be downloaded here:
https://stanfordnlp.github.io/CoreNLP/

Your directory needs to be: .../stanford-corenlp-full-2018-10-05/$.
When in this command, use the following command:
java -mx4g -cp "*" edu.stanford.nlp.pipeline.StanfordCoreNLPServer -preload tokenize,ssplit,pos,lemma,ner,parse,depparse -status_port 9000 -port 9000 -timeout 15000

When this is completed, write in the command line: python3 tagger.py. 
The output is found in the data is files named en.tok.off.pos.tags"""

import wikipedia
import glob
import csv
import sys
import nltk
from nltk.wsd import lesk
from nltk import word_tokenize, pos_tag
from nltk.tree import Tree
from nltk.corpus import wordnet as wn
from nltk.parse import CoreNLPParser
from nltk.metrics import ConfusionMatrix
from collections import Counter


def hypernym_checker(wordlist):
    # This part looks for tags using synsets and hypernyms
    tag_list = []
    pos = 'n'
    # Synsets must have one of the following synsets as a hypernym in 
    # order to receive the tag "sport"
    sport1 = wn.synset("sport.n.01")
    sport2 = wn.synset("sport.n.02")

    # Synsets must have one of the following synsets as a hypernym in 
    # order to receive the tag "natural place"
    places1 = wn.synset("land.n.04")
    places2 = wn.synset("tract.n.01")
    places3 = wn.synset("water.n.02")
    places4 = wn.synset("biome.n.01")
    places5 = wn.synset("formation.n.04")

    # Synsets must have one of the following synsets as a hypernym in 
    # order to receive the tag "animal"
    animal = wn.synset("animal.n.01")

    hypoplaces1 = set([i for i in places1.closure(lambda s:s.hyponyms())])
    hypoplaces2 = set([i for i in places2.closure(lambda s:s.hyponyms())])
    hypoplaces3 = set([i for i in places3.closure(lambda s:s.hyponyms())])
    hypoplaces4 = set([i for i in places4.closure(lambda s:s.hyponyms())])
    hypoplaces5 = set([i for i in places5.closure(lambda s:s.hyponyms())])

    hyposports1 = set([i for i in sport1.closure(lambda s:s.hyponyms())])
    hyposports2 = set([i for i in sport2.closure(lambda s:s.hyponyms())])

    hypoanimal = set([i for i in animal.closure(lambda s:s.hyponyms())])
    
    for word in wordlist:
        syn = wn.synsets(word)
        sent = " ".join(wordlist)
        pos = 'n'

        # Sometimes a word can be ambiguous. To solve this, we use simple_lesk to choose 
        # the right synset.
        definition = lesk(sent, word, pos)
        if definition is not None:
            if definition in hyposports1 or definition in hyposports2:
                tag = "SPORT"
                tag_list.append(tag)
            elif definition in hypoanimal:
                tag = "ANIMAL"
                tag_list.append(tag)
            elif definition in hypoplaces1 or definition in hypoplaces2 or definition in hypoplaces3 or definition in hypoplaces4 or definition in hypoplaces5:
                tag = "NATURAL_PLACE"
                tag_list.append(tag)
            else:
                tag = "O"
                tag_list.append(tag)
        else:
            tag = "O"
            tag_list.append(tag)
        
    return tag_list 


def stanford_tagger(rawlist):
    # This part looks for tags using the stanford tagger
    stanford_tags = []
    ner_tagger = CoreNLPParser(url='http://localhost:9000', tagtype='ner')
    tags = (list(ner_tagger.tag(rawlist)))


    # This next part uses a method that removes items from a list. This is important, otherwise
    # The stanford tag list and the data list sometimes are not the same length.        
    loc = 0
    for item in tags:
        woord = item[0]
        if woord != rawlist[loc]:
                tags.remove(item)
        loc += 1

    for element in tags:
        element = element[1]
        stanford_tags.append(element)
    
    return stanford_tags

def wiki_link(full_tag_list):
    # This part adds wiki links to the words that received tags
    keywords = [('PER', ['born']), ('COU', ['country', 'state']), ('CIT', ['city', 'capital', 'town' 'center']), ('ENT', ['film', 'book', 'novel', 'magazine', 'music', 'song', 'soap', 'album', 'show', 'concert']), ('ORG', ['founded', 'organization', 'organisation']), ('ANI', ['species', 'predator', 'mamal', 'fish', 'reptile', 'habitat']), ('NAT', ['mountain', 'sea' 'located', 'island', 'volcano', 'river', 'water', 'lake']), ('SPO', ['ball', 'team', 'sport'])]
    list_of_urls = []
    # All url's and lack of url's get appended to this list
    for item in full_tag_list:
        lines = item.split("\n")
        linenumber = 0
        lt_list = []
        for i in range(len(lines)):
            try:
                line = lines[linenumber]
                line = line.split()
                title = None
                if len(line) == 6:
                    tag = line[5]
                    title = [line[3]]
                    l = [linenumber]
                    try:
                        # This part chunkes parts of entities together, so that parts of an entity
                        # get the same wikipedia link.
                        while lines[linenumber + 1].split()[5] == tag:
                            title.append(lines[linenumber + 1].split()[3])
                            print(title)
                            linenumber += 1
                            list_of_urls.append(linenumber)
                        else:
                            linenumber += 1
                            lt_listje = [l,tag,title]
                            lt_list.append(lt_listje)
                    except IndexError:
                        lt_listje = [l,tag,title]
                        linenumber += 1
                        lt_list.append(lt_listje)

                else:
                    linenumber += 1
                    list_of_urls.append("")
            except:
                pass

        for l in lt_list:
            lnbs = l[0]
            tag = l[1]
            title = l[2]
            str_title = " ".join(title)
            try:    
                # First, it checks if it can just find the page with the entire title
                wikipedialink = wikipedia.page(str_title)
                list_of_urls.append(wikipedialink.url)
            except:
                # If using only the title doesn't work, it looks for options
                options = wikipedia.search(str_title)
                found = False
                if options != []:
                    for option in options:
                        tagged = False
                        wikitag = None
                        try:
                            # It looks up the summaries of all the options, and 
                            # checks for keywords
                            for word in nltk.word_tokenize(wikipedia.summary(option)):
                                # If the summary conttains keywords of the right tag, it chooses
                                # the option
                                for key_tuple in keywords:
                                    if word in key_tuple[1]:
                                        wikitag = key_tuple[0]
                                        tagged = True
                                        break
                                if tagged == True:
                                    break
                            if wikitag == tag:
                                wikipedialink = wikipedia.page(option)
                                list_of_urls.append(wikipedialink.url)
                                found = True
                                break

                        except:
                            pass

                    if found == False:
                        for option in options:
                            try:
                                wikipedialink = wikipedia.page(option)
                                list_of_urls.append(wikipedialink.url)
                                found = True
                                break
                            except:
                                pass
                        if found == False:
                            list_of_urls.append("Page not found")
                else:
                    list_of_urls.append("Page not found")
    return list_of_urls

def measures(file_path):
    lijstje = []
    lijstje_2 = []
    path_list = glob.glob(file_path +".urls")
    for path in path_list:
        with open(path) as csv_file:
            for line in csv_file:
                line = line.split()
                if len(line) > 5:
                    lijstje.append(line[5])
                else:
                    lijstje.append("x")
   



    path_list = glob.glob(file_path + ".ent")
    for path in path_list:    
        with open(path) as csv_file:
            for line in csv_file:
                line = line.split()
                if len(line) > 5:
                    lijstje_2.append(line[5])
                else:
                    lijstje_2.append("x")



    ref  = lijstje_2
    tagged = lijstje
    cm = ConfusionMatrix(ref, tagged)

    print(cm)

    labels = set('COU CIT NAT PER ORG SPO ANI'.split())

    true_positives = Counter()
    false_negatives = Counter()
    false_positives = Counter()

    for i in labels:
        for j in labels:
            if i == j:
                true_positives[i] += cm[i,j]
            else:
                false_negatives[i] += cm[i,j]
                false_positives[j] += cm[i,j]

    print("TP:", sum(true_positives.values()), true_positives)
    print("FN:", sum(false_negatives.values()), false_negatives)
    print("FP:", sum(false_positives.values()), false_positives)
    print()

    for i in sorted(labels):
        if true_positives[i] == 0:
            fscore = 0
        else:
            precision = true_positives[i] / float(true_positives[i]+false_positives[i])
            recall = true_positives[i] / float(true_positives[i]+false_negatives[i])
            fscore = 2 * (precision * recall) / float(precision + recall)
        print(i, fscore)

def main():
    file_path = "development_data/dev/*/*/en.tok.off.pos"
    path_list = glob.glob(file_path)

    for path in path_list:
        f = open(path)
        # The file from which the data is pulled
        f2 = open(path)
        # Here we open the same path again. If we open f agian instead, the program crashes
        f5 = open(path + ".urls", "w")
        # The file to which our results are sent to

        #Here we create a file that is just the raw text, tokenized, without tags or POS        
        rawlist = []
        final_tags = []
        test_text = []
        
        for regel in f:
            tok_line_list = []
            tok_line = regel.rstrip()
            rawlist.append(tok_line.split(" ")[3])            
        
        stanford_taglist = stanford_tagger(rawlist) # Here we start the stanford NER tagger
            
        hypernyms = hypernym_checker(rawlist) # Here we use hypernyms to discover the remaining tags

        # Now we have to zip together the taglist created bij the stanford NER tagger and the list created by the hypernym checker
        for ner_tag, stanford_tag in zip(stanford_taglist, hypernyms):
                if ner_tag == "O" and stanford_tag != "O":
                    final_tags.append(stanford_tag)
                elif ner_tag == "O" and stanford_tag == "O":
                    final_tags.append("O")
                elif ner_tag != "O" and stanford_tag == "O":
                    final_tags.append(ner_tag)            
                else:
                    final_tags.append(stanford_tag)
        
        final_list = []
        data_with_tags = []
        if len(rawlist) == len(final_tags):
            # Here we zip together the final taglist and the data
            for regel, tag in zip(f2, final_tags):
                regel = regel.rstrip()
                if tag == "SPORT":
                    line_and_tag = "{0} {1}".format(regel, "SPO")
                    final_list.append(line_and_tag)
                    
                elif tag == "ANIMAL":
                    Line_and_tag = "{0} {1}".format(regel, "ANI")
                    final_list.append(line_and_tag)
                    
                elif tag == "NATURAL_PLACE":
                    line_and_tag = "{0} {1}".format(regel, "NAT")
                    final_list.append(line_and_tag)
                    
                elif tag == "LOCATION":
                    line_and_tag = "{0} {1}".format(regel, "COU")
                    final_list.append(line_and_tag)
                    
                elif tag == "COUNTRY":
                    line_and_tag = "{0} {1}".format(regel, "COU")
                    final_list.append(line_and_tag)
                
                elif tag == "STATE_OR_PROVINCE":
                    line_and_tag = "{0} {1}".format(regel, "COU")
                    final_list.append(line_and_tag)
                    
                elif tag == "PERSON":
                    line_and_tag = "{0} {1}".format(regel, "PER")
                    final_list.append(line_and_tag)
                    
                elif tag == "ORGANIZATION":
                    line_and_tag = "{0} {1}".format(regel, "ORG")
                    final_list.append(line_and_tag)
               
                elif tag == "CITY":
                    line_and_tag = "{0} {1}".format(regel, "CIT")
                    final_list.append(line_and_tag)                    
                else:
                    line_and_tag = regel
                    final_list.append(line_and_tag)

                # We now have the data lists plus all the tags. Now we only need to include wikipedia links.
                        
        url_list = wiki_link(final_list)

        for line, url in zip(final_list, url_list):
            line = line.rstrip()
            line_and_url = "{0} {1}".format(line, url)
            f5.write(line_and_url)
            f5.write("\n")
        f5.close()

    results = measures(file_path)


 



   

    
if __name__ == "__main__":
    main()
