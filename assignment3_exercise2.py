#!/usr/bin/python3
# Name: exercise1.py
# A program that complies to exercise 1 of PTA assignment 3.
# Authors: Thimo, Timo and Zhenja
# Date: 06-05-2019


from nltk import word_tokenize, pos_tag
from nltk.corpus import wordnet as wn
import nltk



def main():

    with open("ada_lovelace.txt") as f:
        lines = f.read()
        noun = lambda pos: pos[:2] == 'NN'
        tokenized = word_tokenize(lines)
        nouns = [word for (word, pos) in pos_tag(tokenized) if noun(pos)]
        print(nltk.ne_chunk(nouns, binary=True))

if __name__ == "__main__":
    main()

