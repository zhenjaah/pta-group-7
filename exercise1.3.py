#!/usr/bin/python3
# Name: exercise1.3.py
# A program that complies to exercise 1 of PTA assignment 3.
# Authors: Thimo, Timo and Zhenja
# Date: 06-05-2019


from nltk import word_tokenize, pos_tag
from nltk.corpus import wordnet as wn


def getMaxSim(synsets1, synsets2):
    maxSim = None
    for s1 in synsets1:
        for s2 in synsets2:
            sim = s1.lch_similarity(s2)
            if maxSim == None or maxSim < sim:
                maxSim = sim 
    return maxSim

def main():

    automobileSynsets = wn.synsets("automobile", pos="n")
    carSynsets = wn.synsets("car", pos="n")
    coastSynsets = wn.synsets("coast", pos="n")
    shoreSynsets = wn.synsets("shore", pos="n")
    foodSynsets = wn.synsets("food", pos="n")
    fruitSynsets = wn.synsets("fruit", pos="n")
    journeySynsets = wn.synsets("journey", pos="n")
    monkSynsets = wn.synsets("monk", pos="n")
    slaveSynsets = wn.synsets("slave", pos="n")
    moonSynsets = wn.synsets("moon", pos="n")
    stringSynsets = wn.synsets("string", pos="n")
    print("car-automobile:", "%.2f" % getMaxSim(carSynsets, automobileSynsets))
    print("coast-shore:", "%.2f" % getMaxSim(coastSynsets, shoreSynsets))
    print("food-fruit:", "%.2f" % getMaxSim(foodSynsets, fruitSynsets))
    print("journey-car:", "%.2f" % getMaxSim(journeySynsets, carSynsets))
    print("monk-slave:", "%.2f" % getMaxSim(monkSynsets, slaveSynsets))
    print("moon-string:", "%.2f" % getMaxSim(moonSynsets, stringSynsets))

if __name__ == "__main__":
    main()

