import wikipedia
from nltk import word_tokenize, pos_tag
from nltk.corpus import wordnet
from nltk.wsd import lesk

def main():
    pages = ["China", "Southeast Asia", "Saddam Hussein", "Baghdad", "Police", "Germany", "Italy", "Bulgaria", "Austria", "Europe"]
    total_syn_len = 0 
    total_poly_counter = 0
    synset_amount = {}
    for p in pages:
    
        wikipedia_page = wikipedia.page(p)
        text = wikipedia_page.content
        text_tokenized = word_tokenize(text)
        text_pos = pos_tag(text_tokenized)
        noun = lambda pos: pos == "NN" or pos == "NNS"
        nouns = [word for (word, pos) in pos_tag(text_tokenized) if noun(pos)]
        mono_counter = 0
        poly_counter = 0
        
        for i in nouns:
            if len(wordnet.synsets(i, "n")) == 1:
                mono_counter += 1
                
            elif len(wordnet.synsets(i, "n")) > 1:
                poly_counter += 1
                total_poly_counter += 1
                total_syn_len += len(wordnet.synsets(i, "n"))
                
            if len(wordnet.synsets(i, "n")) not in synset_amount:
                synset_amount[len(wordnet.synsets(i, "n"))] = 1
            else:
                synset_amount[len(wordnet.synsets(i, "n"))] += 1
        print(poly_counter / len(nouns) * 100, "on wikipediapage", p)
        
        sent = word_tokenize(wikipedia.summary(p, sentences=1))
        nns = [word for (word, pos) in pos_tag(sent) if noun(pos)]
        pos = "n"
        for word in nns:
            print(lesk(sent,word,pos), "This one is the right one\n")
            for ss in wordnet.synsets(word, pos):
                print(ss, ss.definition())
        
        
    print("Exercise 5: the noun definition counter is correct.")            
    print(total_syn_len/total_poly_counter)
    print(synset_amount) # way  more than expected for example there are 27 words with 33 synsets
main()
