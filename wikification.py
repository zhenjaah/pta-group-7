# File name: Finalproject.py
# Authors: Durk, Zhenja, Thimo, Timo
# Starting date: 6 Juni

#code voor de stanford server:
#java -mx4g -cp "*" edu.stanford.nlp.pipeline.StanfordCoreNLPServer -preload tokenize,ssplit,pos,lemma,ner,parse,depparse -status_port 9000 -port 9000 -timeout 15000


import glob
import csv
import sys
from nltk.wsd import lesk
from nltk import word_tokenize, pos_tag
from nltk.corpus import wordnet as wn
from nltk.parse import CoreNLPParser
from pywsd.lesk import simple_lesk  # better lesk than the standard


def hypernymOf(synset1, synset2):
    if synset1 == synset2:
        return True
    for hypernym in synset1.hypernyms():
        if synset2 == hypernym:
            return True
        if hypernymOf(hypernym, synset2):
            return True
    return False


def hypernym_checker(wordlist):
    tag_list = []
    pos = 'n'
    sport1 = wn.synset("sport.n.01")
    sport2 = wn.synset("sport.n.02")

    places1 = wn.synset("land.n.04")
    places2 = wn.synset("tract.n.01")
    places3 = wn.synset("water.n.02")
    places4 = wn.synset("biome.n.01")
    places5 = wn.synset("formation.n.04")

    hypoplaces1 = set([i for i in places1.closure(lambda s:s.hyponyms())])
    hypoplaces2 = set([i for i in places2.closure(lambda s:s.hyponyms())])
    hypoplaces3 = set([i for i in places3.closure(lambda s:s.hyponyms())])
    hypoplaces4 = set([i for i in places4.closure(lambda s:s.hyponyms())])
    hypoplaces5 = set([i for i in places5.closure(lambda s:s.hyponyms())])

    hyposports1 = set([i for i in sport1.closure(lambda s:s.hyponyms())])
    hyposports2 = set([i for i in sport2.closure(lambda s:s.hyponyms())])

    animal = wn.synset("animal.n.01")
    hypoanimal = set([i for i in animal.closure(lambda s:s.hyponyms())])

    for word in wordlist:
        syn = wn.synsets(word)
        sent = " ".join(wordlist)
        definition = simple_lesk(sent, word, pos='n')
        if definition is not None:
            if definition in hyposports1 or definition in hyposports2:
                tag = "sport"
                toepol = (word, tag)
                tag_list.append(toepol)
            elif definition in hypoanimal:
                tag = "animal"
                toepol = (word, tag)
                tag_list.append(toepol)
            elif definition in hypoplaces1 or definition in hypoplaces2 or definition in hypoplaces3 or definition in hypoplaces4 or definition in hypoplaces5:
                tag = "natural_places"
                toepol = (word, tag)
                tag_list.append(toepol)
            else:
                tag = "O"
                toepol = (word, tag)
                tag_list.append(toepol)
        else:
            tag = "O"
            toepol = (word, tag)
            tag_list.append(toepol)

    return tag_list


def main():
    # path_list = glob.glob("Finalproject/dev/*/*/*.tok.off.pos") #pathlist voor alle files, er wordt nu voor het testen de eerste drie files gebruikt
    path_list = glob.glob("Finalproject/dev/p05/*/*.tok.off.pos")

    ner_tagger = CoreNLPParser(url='http://localhost:9000', tagtype='ner')

    for path in path_list:
        f = open(path)
        f2 = open(path + ".tags", "w")  # in dit bestand komen alle tags die tot nu toe gevonden kunnen worden
        f3 = open(path + ".raw.txt", "w")  # in dit bestand komt de tekst van de files, achter elkaar
        f4 = open(path)

        rawlist = []
        lijst_tags = ["LOCATION", "COUNTRY", "CITY", "PERSON", "ORGANIZATION"]

        for regel in f:
            regel = regel.rstrip()
            rawlist.append(regel.split(" ")[3])

        # stanford tagger tokenised door de stanford tagger
        # en niet perse hetzelfde als hoe het al getokenized is
        tags = (list(ner_tagger.tag(rawlist)))

        hypernyms = hypernym_checker(rawlist)

        # for tag in stanford_tagged_list:

        print(len(rawlist))  # lengte van hoe de tokens getokenized zijn
        print(len(tags))  # aantal tokens die de stanford tagger er van maakt
        if len(rawlist) == (len(tags)):
            # om een of andere reden kan hier niet dezelfde
            # path gebruikt worden, dus f4 aangemaakt
            for regel, tag in zip(f4, tags):
                regel = regel.rstrip()
                if tag[1] == "LOCATION":
                    print("country tag found and added")
                    print(tag[1] + regel)
                    print(regel + " " + "COU", file=f2)

                if tag[1] == "COUNTRY":
                    print("country tag found and added")
                    print(regel + " " + "COU", file=f2)

                if tag[1] == "PERSON":
                    print("Person tag found and added")
                    print(regel + " " + "PER", file=f2)

                if tag[1] == "ORGANIZATION":
                    print("Org tag found and added")
                    print(regel + " " + "ORG", file=f2)

                if tag[1] == "CITY":
                    print("City tag found and added")
                    print(regel + " " + "CIT", file=f2)
                # als het niet een van de tags is waar de stanford tagger goed,
                # in is, dan komt er een "O" te staan in het file met tags:
                if tag[1] not in lijst_tags:
                    print(tag[1])
                    print(regel + " " + "O", file=f2)

            for regel, tag in zip(f4, hypernyms):
                if tag[1] == "animal":
                    print("animal tag found and added")
                    print(regel + " " + "ANI", file=f2)

                if tag[1] == "natural_places":
                    print("natural place tag found and added")
                    print(regel + " " + "NAT", file=f2)
        else:
            print("")

            # check of tags juist terrecht komen, want de tokenizer
            # van de stanford tagger is anders dan hoe het al getokenized is
            # ik heb een mail van Ian, hoe je dit kan oplossen: via het mergen
            # van lijsten, dit moet ik nog toevoegen
        f.close()
        f2.close()
        f3.close()
        f4.close()
    print(len(rawlist))
    print(hypernyms)


main()
