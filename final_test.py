# File name: tagger.py
# Authors: Durk Betsem, Thimo Huisman, Zhenja Gnezdilov, Timo Strijbis,
# Date: June 17 
"""Usage: For this program to work, you need to have the stanford coreNLP package and have this as your directory.
The stanford package can be downloaded here: https://stanfordnlp.github.io/CoreNLP/

Your directory needs to be: .../stanford-corenlp-full-2018-10-05/$. When in this command, use the following command:
java -mx4g -cp "*" edu.stanford.nlp.pipeline.StanfordCoreNLPServer -preload tokenize,ssplit,pos,lemma,ner,parse,depparse -status_port 9000 -port 9000 -timeout 15000

When this is completed, write in the command line: python3 tagger.py. The output is found in the data is files named en.tok.off.pos.tags"""

import wikipedia
import glob
import csv
import sys
from nltk.wsd import lesk
from nltk import word_tokenize, pos_tag
from nltk.corpus import wordnet as wn
from nltk.parse import CoreNLPParser
import spacy
import glob


def hypernym_checker(wordlist):
    tag_list = []
    pos = 'n'
    #Synsets must have one of the following synsets as a hypernym in order to receive the tag "sport"
    sport1 = wn.synset("sport.n.01")
    sport2 = wn.synset("sport.n.02")

    #Synsets must have one of the following synsets as a hypernym in order to receive the tag "natural place"
    places1 = wn.synset("land.n.04")
    places2 = wn.synset("tract.n.01")
    places3 = wn.synset("water.n.02")
    places4 = wn.synset("biome.n.01")
    places5 = wn.synset("formation.n.04")

    #Synsets must have one of the following synsets as a hypernym in order to receive the tag "animal"
    animal = wn.synset("animal.n.01")

    hypoplaces1 = set([i for i in places1.closure(lambda s:s.hyponyms())])
    hypoplaces2 = set([i for i in places2.closure(lambda s:s.hyponyms())])
    hypoplaces3 = set([i for i in places3.closure(lambda s:s.hyponyms())])
    hypoplaces4 = set([i for i in places4.closure(lambda s:s.hyponyms())])
    hypoplaces5 = set([i for i in places5.closure(lambda s:s.hyponyms())])

    hyposports1 = set([i for i in sport1.closure(lambda s:s.hyponyms())])
    hyposports2 = set([i for i in sport2.closure(lambda s:s.hyponyms())])

    hypoanimal = set([i for i in animal.closure(lambda s:s.hyponyms())])
    
    for word in wordlist:
        syn = wn.synsets(word)
        sent = " ".join(wordlist)
        pos = 'n'

        #Sometimes a word can be ambiguous. To solve this, we use simple_lesk to choose the right synset.
        definition = lesk(sent, word, pos)
        if definition is not None:
            if definition in hyposports1 or definition in hyposports2:
                tag = "SPORT"
                tag_list.append(tag)
            elif definition in hypoanimal:
                tag = "ANIMAL"
                tag_list.append(tag)
            elif definition in hypoplaces1 or definition in hypoplaces2 or definition in hypoplaces3 or definition in hypoplaces4 or definition in hypoplaces5:
                tag = "NATURAL_PLACE"
                tag_list.append(tag)
            else:
                tag = "O"
                tag_list.append(tag)
        else:
            tag = "O"
            tag_list.append(tag)
        
    return tag_list 


def stanford_tagger(rawlist):
    stanford_tags = []
    ner_tagger = CoreNLPParser(url='http://localhost:9000', tagtype='ner')
    tags = (list(ner_tagger.tag(rawlist)))
        
    loc = 0
    for item in tags:
        woord = item[0]
        if woord != rawlist[loc]:
                tags.remove(item)
        loc += 1

    for element in tags:
        element = element[1]
        stanford_tags.append(element)
    
    return stanford_tags

def wikipedia_links(word_list):
    new_list = []
    for i in range(len(word_list)):
        try:
            if type(word_list[i]) == list:#if item is a list item, all the tokens in this item will be used to find the wikipedia page. This link is then added to this item and this item will be appended to a new list.
                link = ""
                for item in word_list[i]:
                    if item[5] != "O":
                        link += "{0} ".format(item[0])
                page = wikipedia.page(link)
                for item in word_list[i]:
                    group = list(item)
                    if group[5] != "O":
                        group.append(page.url)
                    new_list.append(group)
            elif word_list[i][5] != "O":#if item is not a list item, but has a entity, that token will be used to find the wikipedia page. This link is added to the item and this item is then appended to a new list.
                word_group = list(word_list[i])
                page = wikipedia.page(word_group[0])
                word_group.append(page.url)
                new_list.append(word_group)
            else:#if item is not a list item and has no entity, it will be appended to the new list.
                new_list.append(list(word_list[i]))
        except wikipedia.exceptions.DisambiguationError:#these errors may occur when a word is ambigous or if the page does not exist. In those cases, the current item will added to the new list.
            if type(word_list[i]) == list:
                for item in word_list[i]:
                    new_list.append(list(item))
            else:
                new_list.append(list(word_list[i]))
        except wikipedia.exceptions.PageError:
            if type(word_list[i]) == list:
                for item in word_list[i]:
                    new_list.append(list(item))
            else:
                new_list.append(list(word_list[i]))
        except wikipedia.exceptions.WikipediaException:
            for item in word_list[i]:
                new_list.append(list(item))

    return new_list


def main():
    path_list = glob.glob("Finalproject/dev/p05/*/*.tok.off.pos")

    for path in path_list:
        f = open(path)
        f2 = open(path + ".tags", "w")
        f3 = open(path + ".raw.txt", "w") 
        f4 = open(path)
        f5 = open(path + ".tags")

        #Here we create a file that is just the raw text, tokenized, without tags or POS        
        rawlist = []
        final_tags = []
        
        for regel in f:
            regel = regel.rstrip()
            rawlist.append(regel.split(" ")[3])
            
        
        stanford_taglist = stanford_tagger(rawlist) #Here we start the stanford NER tagger
            
        hypernyms = hypernym_checker(rawlist) #Here we use hypernyms to discover the remaining tags

        #Now we have to zip together the taglist created bij the stanford NER tagger and the list created by the hypernym checker
        for item, item2 in zip(stanford_taglist, hypernyms):
                if item == "O" and item2 != "O":
                    final_tags.append(item2)
                elif item == "O" and item2 == "O":
                    final_tags.append("O")
                elif item != "O" and item2 == "O":
                    final_tags.append(item)            
                else:
                    final_tags.append(item2)
         
        error_list = ["/", "(", ")", "<", ">", ";", ":", "'", "?", "[", "]", "{", "}", "@", "!", "#"]
        
        final_list = []
        data_with_tags = []
        if len(rawlist) == len(final_tags):
            #Here we zip together the final taglist and the data
            for regel, tag in zip(f4, final_tags):
                regel = regel.rstrip()
                if tag == "SPORT":
                    asdf = "{0} {1}".format(regel, "SPO")
                    f2.write(asdf)
                    final_list.append(asdf)
                    f2.write("\n")
                    
                elif tag == "ANIMAL":
                    asdf = "{0} {1}".format(regel, "ANI")
                    f2.write(asdf)
                    final_list.append(asdf)
                    f2.write("\n")
                    
                elif tag == "NATURAL_PLACE":
                    asdf = "{0} {1}".format(regel, "NAT")
                    f2.write(asdf)
                    final_list.append(asdf)
                    f2.write("\n")
                    
                elif tag == "LOCATION":
                    asdf = "{0} {1}".format(regel, "COU")
                    f2.write(asdf)
                    final_list.append(asdf)
                    f2.write("\n")
                    
                elif tag == "COUNTRY":
                    asdf = "{0} {1}".format(regel, "COU")
                    f2.write(asdf)
                    final_list.append(asdf)
                    f2.write("\n")
                
                elif tag == "STATE_OR_PROVINCE":
                    asdf = "{0} {1}".format(regel, "COU")
                    f2.write(asdf)
                    final_list.append(asdf)
                    f2.write("\n")
                    
                elif tag == "PERSON":
                    asdf = "{0} {1}".format(regel, "PER")
                    f2.write(asdf)
                    final_list.append(asdf)
                    f2.write("\n")
                    
                elif tag == "ORGANIZATION":
                    asdf = "{0} {1}".format(regel, "ORG")
                    f2.write(asdf)
                    final_list.append(asdf)
                    f2.write("\n")
               
                elif tag == "CITY":
                    asdf = "{0} {1}".format(regel, "CIT")
                    f2.write(asdf)
                    f2.write("\n")
                    
                else:
                    asdf = "{0}".format(regel)
                    f2.write(asdf)
                    final_list.append(asdf)
                    f2.write("\n")

                #We now have the data lists plus all the tags. Now we only need to incluse wikipedia links.
                        
        links = wikipedia_links(final_list)
        print(links)



 



   

    
main()
