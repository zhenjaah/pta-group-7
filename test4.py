#!/usr/bin/python3
# Group 15

import wikipedia
import wikipediaapi
from nltk.tag import StanfordNERTagger
import nltk
import glob
from nltk.wsd import lesk
from nltk.corpus import wordnet

def wiki_link(file):

    keywords = [('PER', ['born']), ('COU', ['country', 'state']), ('CIT', ['city', 'capital', 'town']), ('ENT', ['film', 'book', 'novel', 'magazine', 'music', 'song', 'soap', 'album', 'show', 'concert']), ('ORG', ['founded', 'organization', 'organisation']), ('ANI', ['species', 'habitat']), ('NAT', ['mountain', 'located', 'volcano', 'river', 'water']), ('SPO', ['ball', 'team', 'sport'])]
    lines = file.split("\n")
    linenumber = 0
    lt_list = []
    for i in range(len(lines)):
        try:
            line = lines[linenumber]
            line = line.split()
            title = None
            if len(line) == 6:
                tag = line[5]
                title = [line[3]]
                l = [linenumber]
                try:
                    while lines[linenumber + 1].split()[5] == tag:
                        title.append(lines[linenumber + 1].split()[3])
                        linenumber += 1
                        l.append(linenumber)
                    else:
                        linenumber += 1
                        lt_listje = [l,tag,title]
                        lt_list.append(lt_listje)
                except IndexError:
                    lt_listje = [l,tag,title]
                    linenumber += 1
                    lt_list.append(lt_listje)

            else:
                linenumber += 1
        except:
            pass

    for l in lt_list:
        lnbs = l[0]
        tag = l[1]
        title = l[2]
        str_title = " ".join(title)
        try:
            wikipedialink = wikipedia.page(str_title)
            print("page found")
            l.append(wikipedialink.url)
        except:
            print("ambiquity found")
            options = wikipedia.search(str_title)
            print(options)
            #print(options)
            found = False
            if options != []:
                for option in options:
                    tagged = False
                    wikitag = None
                    print(option)
                    try:
                        for word in nltk.word_tokenize(wikipedia.summary(option)):
                            print("summary works", word)
                            for key_tuple in keywords:
                                if word in key_tuple[1]:
                                    wikitag = key_tuple[0]
                                    tagged = True
                                    break
                            if tagged == True:
                                break
                        if wikitag == tag:
                            wikipedialink = wikipedia.page(option)
                            l.append(wikipedialink.url)
                            found = True
                            break

                    except:
                        pass

                if found == False:
                    for option in options:
                        try:
                            wikipedialink = wikipedia.page(option)
                            l.append(wikipedialink.url)
                            found = True
                            break
                        except:
                            pass
                    if found == False:
                        l.append("Page not found")
            else:
                l.append("Page not found")
    return lt_list

def word_tokenizer(raw):

    raw = raw.split("\n")
    words = []

    try:
        for line in raw:
            line = line.split(" ")
            words.append(line[3])
    except IndexError:
        pass

    return words

def NER(text):

    tagger = StanfordNERTagger('/home/wouter/Documents/pta/final_project/stanford-ner-2018-02-27/classifiers/english.all.3class.distsim.crf.ser.gz', '/home/wouter/Documents/pta/final_project/stanford-ner-2018-02-27/stanford-ner.jar')
    ent_tags = tagger.tag(text.split())
    return ent_tags

def transform_ent(context, word, ent):

    synset = lesk(context, word, 'n')
    if ent == "ORGANIZATION":
        return "ORG"
    if ent == "PERSON":
        return "PER"
    else:
        if synset:
            if ent == "LOCATION":
                for ss in wordnet.synsets(word, 'n'):
                    synpaths = ss.hypernym_paths()
                    for synpath in synpaths:
                        for synset in synpath:
                            name = synset.name()
            
                            if "city" in name or "town" in name or "capital" in name:
                                return "CIT"
                            elif "country" in name or "state" in name or "republic" in name:
                                return "COU"
                        
                    return "NAT"

            elif ent == "O":
                for ss in wordnet.synsets(word, 'n'):
                    synpaths = ss.hypernym_paths()
                    for synpath in synpaths:
                        for synset in synpath:
                            name = synset.name()
                            if "animal" in name:
                                return "ANI"
                            elif "sport" in name:
                                return "SPO"
                            elif "entertainment" in name:
                                return "ENT"
                            else:
                                return ""
            else:
                return ""
        else:
            if ent == "LOCATION":
                return "NAT"
            else:
                return ""


def main():

    for filename in glob.glob("development/*/*/en.tok.off.pos"):
        f = open(filename)
        raw = f.read()
        words = word_tokenizer(raw)
        txt = " ".join(words)
        lines = []
        for line in raw.split("\n"):
            lines.append(line)

        tagged = NER(txt)

        newtext = ""
        
        for word, ent in tagged:
            final_ent = transform_ent(txt, word, ent)
            newtext += lines.pop(0) + " " + final_ent + "\n"
        newnewtext = ""
        newlines = []
        for line in newtext.split("\n"):
            newlines.append(line)

        big_list = wiki_link(newtext)
        l_u = []

        for listje in big_list:
            numbers = listje[0]
            url = listje[3]
            for number in numbers:
                l_u.append((number,url))

        linenumber = 0
        for line in newlines:
            linkcheck = False
            for tup in l_u:
                if linenumber == tup[0]:
                    newnewtext += newlines[linenumber] + " " + tup[1] + "\n"
                    linkcheck = True
                else:
                    pass
            if linkcheck == False:
                newnewtext += newlines[linenumber] + "\n"
            linenumber += 1

        newfile = open(filename + ".ent", "w")
        print(filename)
        newfile.write(newnewtext)


if __name__ == "__main__":
    main()
