import glob
import nltk
import wikipedia
from nltk.tag import StanfordNERTagger
from nltk.wsd import lesk
from nltk.corpus import wordnet
from nltk.collocations import *
from nltk.tree import Tree
from pywsd.lesk import simple_lesk#improved lesk algorithm
import sys

#Don't forget to point the Stanford NER file path to the correct filepath

ent_categories = ('book','magazine','film','concert','movie','song','literature','album','event', 'game','radio','award')

def checkwiki(word):
    try: 
        page = wikipedia.page(word)
        for page_cat in page.categories:
            for ent_cat in ent_categories:
                if ent_cat in page_cat:
                    return True
    except (wikipedia.exceptions.DisambiguationError, wikipedia.exceptions.PageError):
        pass

def get_ent(linelist):   
    newlist = []
    for i in range(len(linelist)):
        try:
            if (linelist[i][0][0].isupper()
            and linelist[i][1] in ["NN", "NNS", "NNP"]
            and linelist[i][2] == 'O'):
                if (linelist[i+2][0][0].isupper() #checks named entity trigram
                and linelist[i+2][1] in ["NN", "NNS", "NNP"]
                and linelist[i+2][2] == 'O'):
                    if checkwiki(linelist[i][0] + linelist[i+1][0] + linelist[i+2][0]):
                        linelist[i][2] = linelist[i+1][2] = linelist[i+2][2] = 'ENT'
                
                elif (linelist[i+1][0][0].isupper()
                and linelist[i+1][1] in ["NN", "NNS", "NNP"]
                and linelist[i+1][2] == 'O'):
                    if checkwiki(linelist[i][0] + linelist[i+1][0]):
                        linelist[i][2] = linelist[i+1][2] = 'ENT'

                elif checkwiki(linelist[i][0]):
                    linelist[i][2] = 'ENT' 

            newlist.append(tuple(linelist[i])) 
        except IndexError:
            pass
        
    return newlist

def locations(word_list, test_sent):
    updated_list = []
    #synsets to check if country
    country = wordnet.synset("country.n.02")
    Geo_area = wordnet.synset("geographical_area.n.01")
    states = wordnet.synset("state.n.01")
    #synsets to check if city
    cities = wordnet.synset("city.n.01")
    for i in range(len(word_list)):
        location_list = []
        if type(word_list[i]) == list:#To check if chunked word
            if len(word_list[i]) == 2:#if chunked word has two words
                if word_list[i][1][1][:2] == "NN" and word_list[i][1][2] == "LOCATION":#check if last word of chunk is a NOUN and tagged with LOCATION by Standford Tagger
                    full_word = "{}_{}".format(word_list[i][0][0], word_list[i][1][0])#changes chunked country into one word for simple_lesk
                    s = simple_lesk(test_sent, full_word, pos='n')#Using simple_lesk for improved word sense disambiguation
                    if s:# check if word was found in worndet
                        instance = s.instance_hypernyms()[0]
                        if hypernymOf(instance, country) or hypernymOf(instance, states) or instance == Geo_area:#if the disambiguated synset s == synset or a hypernyms of synset: change ent to COU for every item in chunk
                            for item in word_list[i]:
                                item[2] = "COU"
                                location_list.append(item)
                        elif hypernymOf(instance, cities):#if the disambiguated synset s == synset or a hypernyms of synset: change ent to CIT for every item in chunk
                            for item in word_list[i]:
                                item[2] = "CIT"
                                location_list.append(item)
                        else:
                            for item in word_list[i]:
                                item[2] = "NAT"
                                location_list.append(item)
                    else:#if word not in wordnet or not hyponym of country,state,geagraphical_area. Used to catch un/little known cities and villages
                        for item in word_list[i]:
                            word_group = list(item)
                            word_group[2] = "CIT"
                            location_list.append(word_group)
                if len(location_list) != 0:
                    updated_list.append(location_list)
                else:
                    for item in word_list[i]:
                        location_list.append(item)
                    if len(location_list) != 0:
                        updated_list.append(location_list)
            elif len(word_list[i]) == 1:#if list item has 1 item
                if word_list[i][0][1][:2] == "NN" and word_list[i][0][2] == "LOCATION":
                    s = lesk(test_sent, word_list[i][0][0], pos='n')
                    if s:# check if word was found in wordnet
                        try:#  when word has no instance it's probably a CIT
                            instance = s.instance_hypernyms()[0]
                            if hypernymOf(instance, country) or hypernymOf(instance, states) or instance == Geo_area:
                                cou_word = list(word_list[i][0])
                                cou_word[2] = "COU"
                                location_list.append(cou_word)
                                updated_list.append(location_list)
                            elif hypernymOf(instance, cities):
                                cit_word = list(word_list[i][0])
                                cit_word[2] = "CIT"
                                location_list.append(cit_word)
                                updated_list.append(location_list)
                            else:# if s not empty and word not country or city than it's a natural place
                                nat_word = list(word_list[i][0])
                                nat_word[2] = "NAT"
                                location_list.append(nat_word)
                                updated_list.append(location_list)
                        except IndexError:# when no instance of synset is found
                            cit_word = list(word_list[i][0])
                            cit_word[2] = "CIT"
                            location_list.append(cit_word)
                            updated_list.append(location_list)
                    else:#if word not in wordnet or not hyponym of country,state,geagraphical_area. Used to catch un/little known cities and villages
                        cit_word = list(word_list[i][0])
                        cit_word[2] = "CIT"
                        location_list.append(cit_word)
                        updated_list.append(location_list)
                else:
                    updated_list.append(word_list[i])
            else:
                updated_list.append(word_list[i])
        else:#This code only executes if item not a list (i.e. tuple)
            if word_list[i][1] =="NN" and word_list[i][2] == "LOCATION":
                s = lesk(test_sent, word_list[i][0], pos='n')
                if s:# check if word was found in worndet
                    instance = s.instance_hypernyms[0]
                    if hypernymOf(instance, country) or hypernymOf(instance, states) or instance == Geo_area:
                        cou_word = list(word_list[i])
                        cou_word[2] = "COU"
                        updated_list.append(tuple(cou_word))
                    elif hypernymOf(instance, cities):
                        cit_word = list(word_list[i])
                        cit_word[2] = "CIT"
                        updated_list.append(tuple(cit_word))
                    else:
                        nat_word = list(word_list[i])
                        nat_word[2] = "NAT"
                        updated_list.append(tuple(nat_word))
                else:#if word not in wordnet or not hyponym of country,state,geagraphical_area. Used to catch un/little known cities and villages
                    cit_word = list(word_list[i])
                    cit_word[2] = "CIT"
                    updated_list.append(tuple(cit_word))
            else:
                updated_list.append(word_list[i])
    return updated_list



def hypernymOf(synset1, synset2):
    if synset1 == synset2:
        return True
    for hypernym in synset1.hypernyms():
        if synset2 == hypernym:
            return True
        if hypernymOf(hypernym, synset2):
            return True
    return False

def get_ani(word_list, test_sent):
    new_list = []
    hypo = lambda s:s.hyponyms()
    animal = wordnet.synset('animal.n.01')
    trues = 0
    for i in range(len(word_list)):
        combo_list = []
        if type(word_list[i]) == list:#checks if item is a list item.
            if len(word_list[i]) == 2:#to work with list items that have 2 words
               if word_list[i][1][1][:2] == "NN" and word_list[i][1][2] == "O":#these lines check in a combination of JJ and NN or NN and NN, if the last noun is a hyponym of animal. If so, all the entries in this list item are given the ANI-entity. The result of this function is a list with list items if multiple tokens belong together and tuples if a token stands alone.
                    word = word_list[i][1][0]
                    sent = test_sent
                    pos = "n"
                    s = lesk(sent, word, pos)
                    for synset in animal.closure(hypo):
                        try:
                            if hypernymOf(synset, s):
                                trues += 1
                        except AttributeError:
                            pass
                    if trues == 1 or trues > 1:
                        for item in word_list[i]:
                            word_group = list(item)
                            word_group[2] = "ANI"
                            combo_list.append(word_group)

              
               if len(combo_list) != 0:
                   new_list.append(combo_list)

               else:#to keep nouns that still belong together in a seperate list item
                   for item in word_list[i]:
                       word_group = list(item)
                       combo_list.append(word_group)
                   if len(combo_list) != 0:
                       new_list.append(combo_list)
            else:#to append the items of list items with a length of 1 to this list
                new_list.append(word_list[i])

        elif type(word_list[i]) == Tree:
            new_list.append(word_list[i].leaves())
        else:#to append all the items that are not a list item.
            new_list.append(word_list[i])

    return new_list

def get_spo(word_list, test_sent):#the result of this function is a list with list items if tokens belong together and tuples if the token stands alone.
    new_list = []
    hypo = lambda s:s.hyponyms()
    sport = wordnet.synset('sport.n.01')
    for i in range(len(word_list)):
        if type(word_list[i]) == list:#if item is a list item, append it to a new list.
            new_list.append(word_list[i])
        else:#these lines check if a noun is a hyponym of sport. If so, this noun is given the SPO-entity.
            trues = 0
            if word_list[i][1][-2:] == "NN" and word_list[i][2] == "O":
                word = word_list[i][0]
                sent = test_sent
                pos = "n"
                s = lesk(sent, word, pos)
                for synset in sport.closure(hypo):
                    try:
                        if hypernymOf(synset, s):
                            trues += 1
                    except AttributeError:
                        pass
                if trues == 1:
                    word_group = list(word_list[i])
                    word_group[2] = "SPO"
                    new_list.append(tuple(word_group))
                else:
                    new_list.append(word_list[i])

            else:
                new_list.append(word_list[i])

    return new_list

def wikipedia_links(word_list, test_sent):
    new_list = []
    for i in range(len(word_list)):
        try:
            if type(word_list[i]) == list:#if item is a list item, all the tokens in this item will be used to find the wikipedia page. This link is then added to this item and this item will be appended to a new list.
                link = ""
                for item in word_list[i]:
                    if item[2] != "O":
                        link += "{0} ".format(item[0])
                page = wikipedia.page(link)
                for item in word_list[i]:
                    group = list(item)
                    if group[2] != "O":
                        group.append(page.url)
                    new_list.append(group)
            elif word_list[i][2] != "O":#if item is not a list item, but has a entity, that token will be used to find the wikipedia page. This link is added to the item and this item is then appended to a new list.
                word_group = list(word_list[i])
                page = wikipedia.page(word_group[0])
                word_group.append(page.url)
                new_list.append(word_group)
            else:#if item is not a list item and has no entity, it will be appended to the new list.
                new_list.append(list(word_list[i]))
        except wikipedia.exceptions.DisambiguationError:#these errors may occur when a word is ambigous or if the page does not exist. In those cases, the current item will added to the new list.
            if type(word_list[i]) == list:
                for item in word_list[i]:
                    new_list.append(list(item))
            else:
                new_list.append(list(word_list[i]))
        except wikipedia.exceptions.PageError:
            if type(word_list[i]) == list:
                for item in word_list[i]:
                    new_list.append(list(item))
            else:
                new_list.append(list(word_list[i]))
        except wikipedia.exceptions.WikipediaException:
            for item in word_list[i]:
                new_list.append(list(item))

    return new_list

def collocations(test_sent):#this function uses ne_chunk to put nouns that belong together in a seperate list item. This item is then appended to a list.
    loc = []
    locs = []
    chuncked = nltk.ne_chunk(test_sent)
    for i in range(len(chuncked)):
        if type(chuncked[i]) == Tree:
            try:
                if chuncked[i+1][1] in ["NN", "NNS", "NNP"]:#to put a extra noun in this list item. For example for Heibei province.
                    loc.append(chuncked[i].leaves() + [chuncked[i+1]])
                else:
                    loc.append(chuncked[i].leaves())
            except IndexError:#if the last token is not a noun, this list item is appended to a list.
                loc.append(chuncked[i])
        elif chuncked[i][1] == "JJ":#places tokens like one-horned rhino in a seperate list item
            try:
                if chuncked[i+1][1] in ["NN", "NNS", "NNP"]:
                    loc.append([chuncked[i], chuncked[i+1]])
                else:
                    loc.append(chuncked[i])
            except IndexError:
                    loc.append([chuncked[i]])
        else:#to eliminate duplicate nouns
            loc.append(chuncked[i])

    for i in range(len(loc)):#this whole function is used to eliminate duplicate entries which may have occured during the last few lines.
        try:
            if type(loc[i]) == list:
                locs.append(loc[i])
                if loc[i+1] in loc[i]:
                    pass
            else:
                if loc[i] in loc[i-1]:
                    pass
                else:
                    locs.append(loc[i])
        except IndexError:
            pass
    return locs

def last_list(word_list):
    new_list = []
    for item in word_list:
        if len(item) == 2:
            for token in item:
                new_list.append(token)
        else:
            new_list.append(item)

    return new_list

def main():
    word_lists = []
    file_sents = []
    filelists = glob.glob('development/*/*/en.tok.off.pos')
    #st = StanfordNERTagger('/root/stanford-ner-2018-02-27/classifiers/english.all.3class.distsim.crf.ser.gz', '/root/stanford-ner-2018-02-27/stanford-ner.jar')
    st = StanfordNERTagger('/home/niekholter/Documents/stanford-ner-2018-02-27/classifiers/english.all.3class.distsim.crf.ser.gz', '/home/niekholter/Documents/stanford-ner-2018-02-27/stanford-ner.jar')
    counter = 0
    for file in filelists:
        counter += 1
        print(file)
        with open(file) as f:
            tok_line_list = []
            for line in f:
                tok_line = line.rstrip()
                tok_split_line = tok_line.split()
                tok_line_list.append(tok_split_line)
            file_sent = [line[3] for line in tok_line_list]
        
        
            st_tags = st.tag(file_sent)
            for i in range(len(tok_line_list)):
                tok_line_list[i].append(st_tags[i][-1])
            word_list = [[line[3], line[4], line[5]] for line in tok_line_list]#creates a tuple that contains the token, its pos-tag and its entity tag
            #ent_tagged = get_ent(word_list)
            firstfirst_tok = get_ent(word_list)
            bigrams = collocations(firstfirst_tok)
            test_sent = " ".join(file_sent)
            first_tok = get_ani(bigrams, test_sent)
            second_tok = get_spo(first_tok, test_sent)
            third_tok = locations(second_tok, test_sent)
            last_tok = wikipedia_links(third_tok, test_sent)
            final_tok = last_list(last_tok)
            for i in range(len(tok_line_list)):
                try:
                    tok_line_list[i][5] = final_tok[i][2][:3]
                    tok_line_list[i].append(final_tok[i][3])
                except IndexError:
                    tok_line_list[i][5] = final_tok[i][2][:3]

            print(tok_line_list)
            print("\n")
            newfile = open(file + ".output","w+")
            for line in tok_line_list:
                newfile.write("{0} {1}".format(" ".join(line), "\n"))
            newfile.close()
            if len(sys.argv) > 1:
                default_counter = int(sys.argv[1])
            else:
                default_counter = 1
            if counter == default_counter:#change amount of files used with command line argument 
                break
main()