#File name: emmanltk.py

import nltk
from nltk.corpus import gutenberg


def main():
    macbeth_Text = gutenberg.raw("shakespeare-macbeth.txt")
    print(macbeth_Text[:289])
    print()
    macbeth_Words = gutenberg.words("shakespeare-macbeth.txt")
    print(macbeth_Words[:30])
    print()
    macbeth_Lines = gutenberg.sents("shakespeare-macbeth.txt")
    print(macbeth_Lines[:4])
    print()
    path = "holmes.txt"
    f = open(path)
    rawText = f.read()
    f.close()
    print(rawText[:165])
    sents = nltk.sent_tokenize(rawText)
    print(sents[:3])
    tokens = []
    for sent in sents:
        tokens += nltk.word_tokenize(sent)

    print(tokens[300:350])
    print()
    bigrams = list(nltk.bigrams(tokens))
    fdist = nltk.FreqDist(bigrams)
    index = 0
    for b, f in fdist.items() :
        print(b, f)
        index += 1
        if index == 20:
            break

main()


