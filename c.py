import glob
import csv
from nltk import word_tokenize, pos_tag

def main():

    CHcounter = 0
    SENcounter = 0
    IDcounter = 0

    path_list = glob.glob("group7/*/*/*.tok.off")
    for path in path_list:
        with open(path) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter = " ")
            for line in csv_reader:
                print(line)

        # onderstaande werkt wel goed voor 1 bestand tegelijk, 
        # nu klopt de output niet helemaal.

    for path in path_list:
        with open(path) as f:
            lines = f.read()
            tokenized = word_tokenize(lines)
            pos = pos_tag(tokenized)
            for tag in pos:
                IDcounter += 1
                if tag[0] in ".?!":
                    SENcounter += 1000
                    IDcounter = 0
                    IDcounter += 1
                print(SENcounter + 1000 + IDcounter, tag[0], tag[1])


main()
