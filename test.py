#!/usr/bin/python3
# Name: exercise1.py
# A program that complies to exercise 1 of PTA assignment 3.
# Authors: Thimo, Timo and Zhenja
# Date: 06-05-2019


from nltk import word_tokenize, pos_tag
from nltk.corpus import wordnet as wn


def hypernymOf(synset1, synset2):
    """ Returns True if synset2 is a hypernym of synset1, or if they are the same synset.
        Returns False otherwise. """
    if synset1 == synset2:
        return True
    for hypernym in synset1.hypernyms():
        if synset2 == hypernym:
            return True
        if hypernymOf(hypernym, synset2):
            return True
    return False


def getMaxSim(synsets1, synsets2):
    maxSim = None
    for s1 in synsets1:
        for s2 in synsets2:
            sim = s1.lch_similarity(s2)
            if maxSim == None or maxSim < sim:
                maxSim = sim 
    return maxSim


def main():

    with open("ada_lovelace.txt") as f:
        lines = f.read()
        noun = lambda pos: pos[:2] == "NN"
        tokenized = word_tokenize(lines)
        nouns = [word for (word, pos) in pos_tag(tokenized) if noun(pos)]
    rel = wn.synsets("relative")
    sci = wn.synsets("science")
    ill = wn.synsets("illness")
    counter = 0

    print("All nouns which refer to relative:")
    for i in nouns:
        for j in wn.synsets(i):
            for k in rel:
                if hypernymOf(j, k):
                    print(i, "\n")
                    counter += 1
    print("Total amount of nouns referring to relative: {}\n".format(counter))
    counter = 0
    print("All nouns which refer to science:")
    for i in nouns:
        for j in wn.synsets(i):
            for k in sci:
                if hypernymOf(j, k):
                    print(i, "\n")
                    counter += 1
    print("Total amount of nouns referring to science: {}\n".format(counter))
    counter = 0
    print("All nouns which refer to illness:")
    for i in nouns:
        for j in wn.synsets(i):
            for k in ill:
                if hypernymOf(j, k):
                    print(i, "\n")
                    counter += 1
    print("Total amount of nouns referring to illness: {}\n".format(counter))  
    
    hypernyms_amount = []
    for i in nouns:
        hyp_counter = 0
        n = wn.synsets(i)
        for j in n:
            hyp_counter += 1
        hypernyms_amount.append(hyp_counter)
    print('The amount of hypernyms per noun')
    print(hypernyms_amount, "\n")
    
    one_hyp_counter = 0
    multi_hyp_counter = 0
    average_hyps=0
    for j in hypernyms_amount:
        if j == 1:
            one_hyp_counter +=1
        if j > 1:
            multi_hyp_counter +=1
        average_hyps += j
    average_hyps = average_hyps / len(hypernyms_amount)
    print('Amount of nouns with one hypernym:')
    print(one_hyp_counter, "\n")
    print('Amount of nouns with more than one hypernym:')
    print(multi_hyp_counter, "\n")
    print('Average amount of hypernyms per noun')
    print("%.2f" % average_hyps)

    automobileSynsets = wn.synsets("automobile", pos="n")
    carSynsets = wn.synsets("car", pos="n")
    coastSynsets = wn.synsets("coast", pos="n")
    shoreSynsets = wn.synsets("shore", pos="n")
    foodSynsets = wn.synsets("food", pos="n")
    fruitSynsets = wn.synsets("fruit", pos="n")
    journeySynsets = wn.synsets("journey", pos="n")
    monkSynsets = wn.synsets("monk", pos="n")
    slaveSynsets = wn.synsets("slave", pos="n")
    moonSynsets = wn.synsets("moon", pos="n")
    stringSynsets = wn.synsets("string", pos="n")
    print("\nWordNet similarity:")
    print("\ncar-automobile:", "%.2f" % getMaxSim(carSynsets, automobileSynsets))
    print("coast-shore:", "%.2f" % getMaxSim(coastSynsets, shoreSynsets))
    print("food-fruit:", "%.2f" % getMaxSim(foodSynsets, fruitSynsets))
    print("journey-car:", "%.2f" % getMaxSim(journeySynsets, carSynsets))
    print("monk-slave:", "%.2f" % getMaxSim(monkSynsets, slaveSynsets))
    print("moon-string:", "%.2f" % getMaxSim(moonSynsets, stringSynsets))               



if __name__ == "__main__":
    main()

